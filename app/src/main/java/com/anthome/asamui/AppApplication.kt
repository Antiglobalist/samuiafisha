package com.anthome.asamui

import com.anthome.asamui.di.component.DaggerAppComponent
import com.instabug.library.Instabug
import com.instabug.library.invocation.InstabugInvocationEvent
import dagger.android.AndroidInjector
import dagger.android.support.DaggerApplication


class AppApplication : DaggerApplication() {

    override fun onCreate() {
        super.onCreate()
        Instabug.Builder(this, getString(R.string.instabug_app_id))
                .setInvocationEvent(InstabugInvocationEvent.SHAKE)
                .build();
    }

    override fun applicationInjector(): AndroidInjector<out DaggerApplication> {
        val appComponent = DaggerAppComponent.builder().application(this).build()
        appComponent.inject(this)
        return appComponent
    }
}