package com.anthome.asamui.ui.screens


import dagger.android.support.DaggerFragment

open class BaseFragment : DaggerFragment()
