package com.anthome.asamui.ui.utils


interface OnItemClickListener<T> {
    fun onItemClick(position: Int, item: T)
}