package com.anthome.asamui.ui.screens.events

import android.os.Bundle
import com.anthome.asamui.ui.screens.BaseActivity

class EventsListActivity : BaseActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        savedInstanceState ?:
                supportFragmentManager.beginTransaction()
                        .replace(android.R.id.content, EventsListFragment(), null)
                        .commit()
    }
}
