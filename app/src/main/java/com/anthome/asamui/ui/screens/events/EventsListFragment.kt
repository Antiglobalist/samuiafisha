package com.anthome.asamui.ui.screens.events

import android.arch.lifecycle.Observer
import android.arch.lifecycle.ViewModelProviders
import android.databinding.DataBindingUtil
import android.os.Bundle
import android.support.v4.content.ContextCompat
import android.support.v7.widget.DividerItemDecoration
import android.support.v7.widget.LinearLayoutManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.anthome.asamui.BR
import com.anthome.asamui.R
import com.anthome.asamui.core.common.ListResult
import com.anthome.asamui.core.model.Event
import com.anthome.asamui.databinding.FragmentNewsListBinding
import com.anthome.asamui.ui.screens.BaseFragment
import com.anthome.asamui.ui.utils.AutoClearedValue
import com.anthome.asamui.ui.utils.SimpleBindingAdapter
import com.anthome.asamui.ui.viewmodel.ProcessViewModelState
import com.anthome.asamui.ui.viewmodel.ViewModelFactory
import com.anthome.asamui.ui.widget.StatesView
import javax.inject.Inject


class EventsListFragment : BaseFragment() {

    @Inject
    lateinit var viewModelFactory: ViewModelFactory

    private lateinit var viewModel: EventsListViewModel
    private lateinit var bindingHolder: AutoClearedValue<FragmentNewsListBinding>
    private lateinit var adapterHolder: AutoClearedValue<SimpleBindingAdapter<Event>>

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        viewModel = ViewModelProviders.of(this, viewModelFactory).get(EventsListViewModel::class.java)
        viewModel.viewModelState.observe(this, Observer<ProcessViewModelState> {
            bindingHolder.get()?.state = it
        })
        viewModel.events.observe(this, Observer<ListResult<Event>> {
            adapterHolder.get()?.items = it?.list
            // bindingHolder.get()?.executePendingBindings()
        })
        viewModel.refresh()
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val view = DataBindingUtil.inflate<FragmentNewsListBinding>(inflater,
                R.layout.fragment_news_list, container, false)
        view.list.layoutManager = LinearLayoutManager(context)
        view.list.addItemDecoration(DividerItemDecoration(context, DividerItemDecoration.VERTICAL).apply {
            this.setDrawable(ContextCompat.getDrawable(context!!, R.drawable.divider_empty)!!)
        })
        val adapter = SimpleBindingAdapter<Event>(R.layout.item_news, BR.itemVM)
        view.list.adapter = adapter
        view.refresher.setOnRefreshListener {
            viewModel.refresh()
        }
        view.stateView.setRepeatClickListener(object : StatesView.OnRepeatClickListener {
            override fun onRepeatClicked() {
                viewModel.refresh()
            }
        })
        adapterHolder = AutoClearedValue(this, adapter)
        bindingHolder = AutoClearedValue(this, view)
        return view.root
    }
}