package com.anthome.asamui.ui.utils


import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.ViewGroup

class SimpleBindingAdapter<T : Any>(private val holderLayout: Int, private val variableId: Int) :
        RecyclerView.Adapter<BindingHolder<T>>() {

    var items: List<T>? = null
        set(value) {
            field = value
            notifyDataSetChanged()
        }

    var onItemClickListener: OnItemClickListener<T>? = null

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): BindingHolder<T> {
        val v = LayoutInflater.from(parent.context)
                .inflate(holderLayout, parent, false)
        return BindingHolder(v, onItemClickListener, variableId)
    }

    override fun onBindViewHolder(holder: BindingHolder<T>, position: Int) {
        val item = items!![position]
        holder.bind(item)
    }

    override fun getItemCount(): Int {
        return items?.size ?: 0
    }
}