package com.anthome.asamui.ui.viewmodel


enum class ProcessViewModelState {
    NONE,
    WAITING,
    EMPTY,
    DATA,
    LOADING,
    ERROR
}

/*@BindingConversion
fun convertResourceStateToVMState(value: Resource.Status): ProcessViewModelState {
    return when(value){
        Resource.Status.LOADING ->
    }
}*/

