package com.anthome.asamui.ui.widget

import android.content.Context
import android.support.v4.view.PagerAdapter
import android.support.v4.view.ViewPager
import android.util.AttributeSet
import android.view.View
import android.view.ViewGroup
import com.anthome.asamui.core.model.Event


class PhotoListViewerView : ViewPager {

    private var photos: ArrayList<Event.PhotoEvent>? = null
    private val pager: PhotoAdapter = PhotoAdapter()

    constructor(context: Context) : super(context)
    constructor(context: Context, attrs: AttributeSet?) : super(context, attrs)

    init {
        /*if(isInEditMode){
            photos = ArrayList(1)
            photos!!.add("https://www.istockphoto.com/resources/images/PhotoFTLP/img_82250973.jpg")
        }*/
        adapter = pager

    }

    inner class PhotoAdapter : PagerAdapter() {

        override fun instantiateItem(container: ViewGroup, position: Int): Any {
            return super.instantiateItem(container, position)
        }

        override fun isViewFromObject(view: View, `object`: Any) = view == `object`

        override fun getCount() = photos?.size ?: 0
    }
}