package com.anthome.asamui.ui.screens.events

import android.support.v4.app.Fragment
import dagger.Binds
import dagger.Module

@Module
abstract class EventsListFragmentModule {

    @Binds
    internal abstract fun contributeEventsListFragment(eventsListFragment: EventsListFragment): Fragment
}