package com.anthome.asamui.ui.widget

import android.content.Context
import android.databinding.BindingAdapter
import android.graphics.drawable.Drawable
import android.util.AttributeSet
import android.view.View
import android.widget.ImageView
import com.anthome.asamui.R
import com.anthome.asamui.utils.extensions.getDrawable
import com.anthome.asamui.utils.extensions.getPixelSize
import com.anthome.asamui.utils.extensions.getStateAnimator
import kotlin.properties.Delegates

@BindingAdapter("liked")
fun setLiked(view: LikeView, value: Boolean) {
    if (view.liked != value) {
        view.liked = value
    }
}

class LikeView @JvmOverloads constructor(context: Context,
                           attrs: AttributeSet? = null,
                           defStyleAttr: Int = 0) :
        ImageView(context, attrs, defStyleAttr),
        View.OnClickListener {

    companion object {
        var EMPTY_LIKE: Drawable? = null
        var FILL_LIKE: Drawable? = null
        var SIZE: Int = 0
    }

    var likeChangeListener: OnLikeChangeListener? = null;

    var liked by Delegates.observable(false) {
        pr, old, new ->
        updateLikeState()
    }

    init {
        if (EMPTY_LIKE == null) {
            EMPTY_LIKE = getDrawable(R.drawable.ic_like_empty)
            FILL_LIKE = getDrawable(R.drawable.ic_like)
            SIZE = getPixelSize(R.dimen.dp40)
        }
        scaleType = ScaleType.CENTER
        setImageDrawable(EMPTY_LIKE)
        minimumWidth = SIZE
        minimumHeight = SIZE
        stateListAnimator = getStateAnimator(R.animator.like_view_animator)
        setOnClickListener(this)
    }

    private fun updateLikeState() {
        setImageDrawable(if (liked) FILL_LIKE else EMPTY_LIKE)
    }

    override fun onClick(v: View?) {
        liked = !liked
        likeChangeListener?.onLikeStateChanged(liked)
        updateLikeState()
    }

    interface OnLikeChangeListener {
        fun onLikeStateChanged(liked: Boolean)
    }
}