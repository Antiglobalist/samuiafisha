package com.anthome.asamui.ui.screens.events

import android.arch.lifecycle.LiveData
import android.arch.lifecycle.MutableLiveData
import android.arch.lifecycle.Transformations
import android.arch.lifecycle.ViewModel
import com.anthome.asamui.core.Resource
import com.anthome.asamui.core.common.ListResult
import com.anthome.asamui.core.model.Event
import com.anthome.asamui.core.repository.EventsRepository
import com.anthome.asamui.ui.viewmodel.ProcessViewModelState
import javax.inject.Inject


class EventsListViewModel
@Inject constructor(private val repository: EventsRepository) : ViewModel() {

    val events: LiveData<ListResult<Event>>
    var viewModelState: LiveData<ProcessViewModelState>
    var requaredOffset = MutableLiveData<Int>()

    init {
        val result = Transformations.switchMap(requaredOffset) {
            repository.getEvents(it)
        }

        events = Transformations.map(result) {
            result.value?.data
        }
        viewModelState = Transformations.map(result) {
            when (it.status) {
                Resource.Status.ERROR -> if (it.data?.list != null && it.data.list.isNotEmpty())
                    ProcessViewModelState.DATA else ProcessViewModelState.ERROR
                Resource.Status.SUCCESS -> if (it.data?.list != null && it.data.list.isNotEmpty())
                    ProcessViewModelState.DATA else ProcessViewModelState.EMPTY
                Resource.Status.LOADING ->
                    if (it.data?.list != null && it.data.list.isNotEmpty())
                        ProcessViewModelState.LOADING
                    else ProcessViewModelState.WAITING
                else -> ProcessViewModelState.WAITING
            }
        }
    }

    fun requestMoreData(offset: Int) {
        requaredOffset.value = offset
    }

    fun refresh() {
        requestMoreData(0)
    }

    fun changeLikeState(event: Event){

    }
}