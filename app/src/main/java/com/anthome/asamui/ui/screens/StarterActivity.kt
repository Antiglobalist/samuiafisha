package com.anthome.asamui.ui.screens

import android.os.Bundle
import com.anthome.asamui.R

class StarterActivity : BaseActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_starter)
    }
}
