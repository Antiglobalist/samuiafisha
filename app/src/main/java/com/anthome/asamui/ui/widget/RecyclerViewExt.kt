package com.anthome.asamui.ui.widget

import android.content.Context
import android.support.v7.widget.RecyclerView
import android.util.AttributeSet
import android.view.MotionEvent
import com.anthome.asamui.R
import com.anthome.asamui.utils.GlideApp
import com.bumptech.glide.RequestManager
import kotlin.properties.Delegates


class RecyclerViewExt @JvmOverloads constructor(context: Context,
                                                attrs: AttributeSet? = null,
                                                defStyleAttr: Int = 0) :
        RecyclerView(context, attrs, defStyleAttr) {

    private var isDestroy = false
    private var manager: RequestManager? = null
    private var scrollListener: RecyclerView.OnScrollListener? = null

    var enableTouchControl = false
    var enableImageLoadControl by Delegates.observable(false) { pr, old, new ->
        if (old != new) {
            changemageControlState(new)
        }
    }

    init {
        if (attrs != null) {
            val params = context.theme.obtainStyledAttributes(
                    attrs,
                    R.styleable.RecyclerViewExt,
                    0, 0)

            try {
                enableImageLoadControl = params.getBoolean(R.styleable.RecyclerViewExt_rve_image_control_enable, false)
                enableTouchControl = params.getBoolean(R.styleable.RecyclerViewExt_rve_touch_control_enable, false)
            } finally {
                params.recycle()
            }
        }
    }

    private fun changemageControlState(enable: Boolean) {
        if (enable) {
            manager = GlideApp.with(this)
            scrollListener = object : RecyclerView.OnScrollListener() {
                override fun onScrolled(recyclerView: RecyclerView?, dx: Int, dy: Int) {
                    if (dy > 40 || dy < -40) {
                        if (!isDestroy and !manager!!.isPaused()) {
                            manager!!.pauseRequests()
                        }
                    } else {
                        if (!isDestroy and manager!!.isPaused()) {
                            manager!!.resumeRequests()
                        }
                    }
                    super.onScrolled(recyclerView, dx, dy)
                }

                override fun onScrollStateChanged(recyclerView: RecyclerView?, newState: Int) {
                    if (newState == RecyclerView.SCROLL_STATE_IDLE || newState == RecyclerView.SCROLL_STATE_DRAGGING) {
                        if (!isDestroy and manager!!.isPaused()) {
                            manager!!.resumeRequests()
                        }
                    }
                    super.onScrollStateChanged(recyclerView, newState)
                }
            }
            addOnScrollListener(scrollListener)
        } else {
            manager?.resumeRequests()
            manager = null
            removeOnScrollListener(scrollListener)
            scrollListener = null
        }
    }

    override fun onDetachedFromWindow() {
        super.onDetachedFromWindow()
        isDestroy = true
    }

    override fun onInterceptTouchEvent(e: MotionEvent): Boolean {
        return super.onInterceptTouchEvent(e) or (enableTouchControl and (e.pointerCount >= 2))
    }

}