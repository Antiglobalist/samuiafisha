package com.anthome.asamui.ui.widget

import android.content.Context
import android.util.AttributeSet
import android.widget.FrameLayout
import com.anthome.asamui.core.model.Photo
import kotlin.properties.Delegates


class PhotoStateView @JvmOverloads constructor(context: Context,
                                               attrs: AttributeSet? = null,
                                               defStyleAttr: Int = 0) :
        FrameLayout(context, attrs, defStyleAttr) {

    var photo = Delegates.observable<Photo?>(null){
        pr, old, new ->

    }

    init {

    }
}