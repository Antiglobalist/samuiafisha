package com.anthome.asamui.ui.utils

import android.databinding.DataBindingUtil
import android.databinding.ViewDataBinding
import android.support.v7.widget.RecyclerView
import android.view.View


class BindingHolder<T : Any>(
        val view: View,
        var onItemClickListener: OnItemClickListener<T>? = null,
        val variableId: Int)
    : RecyclerView.ViewHolder(view) {

    lateinit var model: T
    val binding: ViewDataBinding

    init {
        binding = DataBindingUtil.bind(view)!!
        binding.root.setOnClickListener {
            onItemClickListener?.onItemClick(adapterPosition, model)
        }
    }

    fun bind(model: T){
        this.model = model
        binding.setVariable(variableId, model)
    }
}