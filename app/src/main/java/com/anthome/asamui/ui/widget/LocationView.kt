package com.anthome.asamui.ui.widget

import android.content.Context
import android.content.Intent
import android.databinding.BindingAdapter
import android.net.Uri
import android.support.v4.content.ContextCompat.startActivity
import android.support.v7.widget.AppCompatTextView
import android.util.AttributeSet
import com.anthome.asamui.core.model.Event
import com.anthome.asamui.utils.extensions.emptyString
import kotlin.properties.Delegates


@BindingAdapter("location")
fun setLocation(view: LocationView, value: Event.Location) {
    view.location = value
}

class LocationView @JvmOverloads constructor(context: Context,
                                             attrs: AttributeSet? = null,
                                             defStyleAttr: Int = 0) :
        AppCompatTextView(context, attrs, defStyleAttr) {

    var location by Delegates.observable<Event.Location?>(null) { pr, old, new ->
        text = new?.name ?: emptyString
    }

    init {
        setOnClickListener {
            if (location != null) {
                val intent = Intent(android.content.Intent.ACTION_VIEW, Uri.parse("https://www.google.com/maps/search/?api=1&query="
                        + location!!.latitude + "," + location!!.longitude))
                startActivity(context, intent, null)
            }
        }
    }
}