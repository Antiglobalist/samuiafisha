package com.anthome.asamui.ui.widget

import android.content.Context
import android.databinding.BindingAdapter
import android.os.Build
import android.support.annotation.StringRes
import android.support.v4.content.ContextCompat
import android.transition.TransitionManager
import android.util.AttributeSet
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.FrameLayout
import com.anthome.asamui.R
import com.anthome.asamui.ui.viewmodel.ProcessViewModelState
import kotlinx.android.synthetic.main.component_view_states.view.*
import kotlin.properties.Delegates

class StatesView @JvmOverloads constructor(context: Context,
                                           attrs: AttributeSet? = null,
                                           defStyleAttr: Int = 0) :
        FrameLayout(context, attrs, defStyleAttr), View.OnAttachStateChangeListener {

    private lateinit var mContent: View

    private var mContentId: Int = 0;
    private var mDarkStyle: Boolean = false;

    private var inited = false;

    var currentState by Delegates.observable(ViewState.Hide) { pr, old, new ->
        if(inited) {
            updateState()
        }
    }

    private var mRepeatClickListener: OnRepeatClickListener? = null

    init {
        if (attrs != null) {
            val params = context.theme.obtainStyledAttributes(
                    attrs,
                    R.styleable.StatesView,
                    0, 0)

            try {
                mContentId = params.getResourceId(R.styleable.StatesView_sv_content, 0)
                mDarkStyle = params.getBoolean(R.styleable.StatesView_sv_dark_enabled, false)
            } finally {
                params.recycle()
            }
        }

        LayoutInflater.from(context).inflate(R.layout.component_view_states, this, true)
        isClickable = true
        setupStyle()
    }

    override fun onViewDetachedFromWindow(v: View?) {
        initContent()
    }

    override fun onViewAttachedToWindow(v: View?) {

    }

    private fun setupStyle() {
        if (mDarkStyle) {
            setBackgroundColor(ContextCompat.getColor(context, R.color.black))
            empty.setTextColor(ContextCompat.getColor(context, R.color.whiteText))
            error_text.setTextColor(ContextCompat.getColor(context, R.color.whiteText))
        } else {
            setBackgroundColor(ContextCompat.getColor(context, R.color.white))
            empty.setTextColor(ContextCompat.getColor(context, R.color.blackSubText))
            error_text.setTextColor(ContextCompat.getColor(context, R.color.blackSubText))
        }
    }

    override fun onAttachedToWindow() {
        super.onAttachedToWindow()
        initContent()
    }

    fun initContent() {
        if (parent is ViewGroup && mContentId > 0) {
            mContent = (parent as ViewGroup).findViewById(mContentId)
        }
        inited = true;
    }

    fun activateDarkStyle() {
        mDarkStyle = true
        setupStyle()
    }

    fun activateLightStyle() {
        mDarkStyle = false
        setupStyle()
    }

    fun setRepeatClickListener(repeatClickListener: OnRepeatClickListener) {
        mRepeatClickListener = repeatClickListener
    }

    fun setErrorText(@StringRes text: Int) {
        error_text.setText(text)
    }

    fun setErrorText(text: String) {
        error_text.text = text
    }

    fun setEmptyText(@StringRes text: Int) {
        empty.setText(text)
    }

    fun setEmptyText(text: String) {
        empty.text = text
    }

    internal fun onClickRepeat() {
        if (mRepeatClickListener != null) {
            mRepeatClickListener!!.onRepeatClicked()
        }
    }

    fun setContentView(content: View) {
        mContent = content
    }

    private fun updateState() {
        TransitionManager.beginDelayedTransition(this)

        if (currentState == ViewState.Hide) {
            visibility = View.GONE
            mContent.visibility = View.VISIBLE
        } else {
            visibility = View.VISIBLE
            mContent.visibility = View.GONE

            empty!!.visibility = View.GONE
            progress!!.visibility = View.GONE
            error!!.visibility = View.GONE

            when (currentState) {
                StatesView.ViewState.Progress -> progress!!.visibility = View.VISIBLE
                StatesView.ViewState.Empty -> empty!!.visibility = View.VISIBLE
                StatesView.ViewState.Error -> error!!.visibility = View.VISIBLE
            }
        }
        if (Build.VERSION.SDK_INT >= 23) {
            TransitionManager.endTransitions(this)
        }
    }

    enum class ViewState {
        Hide,
        Progress,
        Empty,
        Error
    }

    interface OnRepeatClickListener {
        fun onRepeatClicked()
    }
}


@BindingAdapter("state")
fun setState(view: StatesView, state: ProcessViewModelState?) {
    view.currentState = when (state) {
        ProcessViewModelState.NONE, ProcessViewModelState.LOADING,
        ProcessViewModelState.DATA -> StatesView.ViewState.Hide
        ProcessViewModelState.ERROR -> StatesView.ViewState.Error
        ProcessViewModelState.EMPTY -> StatesView.ViewState.Empty
        ProcessViewModelState.WAITING -> StatesView.ViewState.Progress
        else -> StatesView.ViewState.Hide
    }
}