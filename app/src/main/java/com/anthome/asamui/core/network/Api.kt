package com.anthome.asamui.core.network

import android.arch.lifecycle.LiveData
import com.anthome.asamui.core.model.Event
import com.anthome.asamui.core.network.model.PagesResponse
import retrofit2.http.GET
import retrofit2.http.POST
import retrofit2.http.Query


interface Api {

    @GET("v1/events")
    fun getEvents(@Query("offset") offset: Int):
            LiveData<ApiResponse<PagesResponse<List<Event>>>>

    @POST("v1/like")
    fun sendLike(@Query("id") id: String, @Query("date") date: String,
                 @Query("uuid") uuid: String):
            LiveData<ApiResponse<Event>>
}