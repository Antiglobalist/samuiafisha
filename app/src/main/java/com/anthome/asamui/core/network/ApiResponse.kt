package com.anthome.asamui.core.network


import androidx.text.trimmedLength
import retrofit2.Response

/**
 * Common class used by API responses.
 *
 * @param <T>
</T> */
class ApiResponse<T> {
    val code: Int
    val body: T?
    val errorMessage: String?

    val isSuccessful: Boolean
        get() = code >= 200 && code < 300

    constructor(error: Throwable) {
        code = 500
        body = null
        errorMessage = error.message
    }

    constructor(response: Response<T>) {
        code = response.code()
        if (response.isSuccessful) {
            body = response.body()
            errorMessage = null
        } else {
            var message: String? = response.errorBody()?.string()
            if (message?.trimmedLength() == 0) {
                message = response.message()
            }
            errorMessage = message
            body = null
        }
    }
}