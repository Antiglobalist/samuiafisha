package com.anthome.asamui.core.data.utils

import java.lang.reflect.ParameterizedType
import java.lang.reflect.Type
import java.util.*

class ListParameterizedType (private val type: Type) : ParameterizedType {

    override fun getActualTypeArguments(): Array<Type> {
        return arrayOf(type)
    }

    override fun getRawType(): Type {
        return ArrayList::class.java
    }

    override fun getOwnerType(): Type? {
        return null
    }
}