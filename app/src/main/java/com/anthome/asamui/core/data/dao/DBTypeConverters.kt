package com.anthome.asamui.core.data.dao

import android.arch.persistence.room.TypeConverter
import android.text.TextUtils
import com.anthome.asamui.utils.extensions.emptyString

internal object DBTypeConverters {

    private val SEPARATE = ","

    @TypeConverter
    fun stringToStringList(data: String?): List<String> {
        return if (data == null) {
            emptyList()
        } else if (data.contains(SEPARATE)) {
            data.split(SEPARATE.toRegex())
        } else {
            listOf(data)
        }
    }

    @TypeConverter
    fun stringListToString(list: List<String>?): String {
        return if (list == null || list.size == 0) {
            emptyString
        } else {
            TextUtils.join(SEPARATE, list)
        }
    }
}