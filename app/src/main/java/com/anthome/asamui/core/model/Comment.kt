package com.anthome.asamui.core.model

import com.anthome.asamui.utils.extensions.emptyString
import com.google.gson.annotations.SerializedName

data class Comment(
        @SerializedName("date")
        var date: String = emptyString, //06.03.2018 11:38:38
        @SerializedName("user")
        var user: String = emptyString, //sergeyyyy
        @SerializedName("useruuid")
        var useruuid: String = emptyString, //0F210FEC-57CA-49BC-8967-4ED45EF3F5C1
        @SerializedName("text")
        var text: String = emptyString, //Ппп
        @SerializedName("id")
        var id: Int = 0//000000016
){
    constructor() : this(id = 0)
}
