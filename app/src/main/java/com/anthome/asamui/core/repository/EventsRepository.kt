package com.anthome.asamui.core.repository

import android.arch.lifecycle.LiveData
import android.arch.lifecycle.Transformations
import com.anthome.asamui.core.AppExecutors
import com.anthome.asamui.core.Resource
import com.anthome.asamui.core.common.ListResult
import com.anthome.asamui.core.data.dao.EventsDao
import com.anthome.asamui.core.model.Event
import com.anthome.asamui.core.network.Api
import com.anthome.asamui.core.network.model.PagesResponse
import com.anthome.asamui.utils.extensions.notNull
import javax.inject.Inject


class EventsRepository
@Inject constructor(val executors: AppExecutors,
                    val api: Api,
                    val eventsDao: EventsDao) {

    private var cacheModeEnable = false;

    fun getEvents(offset: Int): LiveData<Resource<ListResult<Event>>> {
        if (offset == 0) cacheModeEnable = false
        return object : NetworkBoundResource<ListResult<Event>, PagesResponse<List<Event>>>(executors) {
            override fun saveCallResult(item: PagesResponse<List<Event>>) {
                item.data.forEach {
                    notNull(it.location) {
                        it.latitude = it.latitude.replace(",", ".")
                        it.longitude = it.longitude.replace(",", ".")
                    }
                }
                eventsDao.insert(item.data)
            }

            override fun shouldFetch(data: ListResult<Event>?) = !cacheModeEnable

            override fun loadFromDb(preFetch: Boolean): LiveData<ListResult<Event>> {
                val count = offset + defaultPageSize
                return Transformations.map(eventsDao.getEvents(0, count)) {
                    ListResult(it, !preFetch || count == it.size, !preFetch)
                }
            }

            override fun createCall() = api.getEvents(offset)

            override fun onFetchFailed() {
                super.onFetchFailed()
                cacheModeEnable = true
            }

        }.asLiveData()
    }

    fun changeLike(event: Event){
        
    }
}