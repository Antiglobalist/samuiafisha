package com.anthome.asamui.core.network.model

import com.google.gson.annotations.SerializedName


class PagesResponse<Data : Any> {

    @SerializedName("data")
    lateinit var data: Data

    @SerializedName("meta")
    lateinit var meta: Meta
}