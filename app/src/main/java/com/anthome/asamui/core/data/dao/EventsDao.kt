package com.anthome.asamui.core.data.dao

import android.arch.lifecycle.LiveData
import android.arch.persistence.room.Dao
import android.arch.persistence.room.Insert
import android.arch.persistence.room.OnConflictStrategy
import android.arch.persistence.room.Query
import com.anthome.asamui.core.model.Event


@Dao
interface EventsDao {
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insert(posts: List<Event>)

    @Query("SELECT * FROM events ORDER BY date LIMIT :count OFFSET :offset")
    fun getEvents(offset: Int, count: Int): LiveData<List<Event>>
}