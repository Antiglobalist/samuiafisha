package com.anthome.asamui.core.repository


import android.arch.lifecycle.LiveData
import android.arch.lifecycle.MediatorLiveData
import android.support.annotation.MainThread
import android.support.annotation.WorkerThread
import com.anthome.asamui.core.AppExecutors
import com.anthome.asamui.core.Resource
import com.anthome.asamui.core.network.ApiResponse

const val defaultPageSize = 30

abstract class NetworkBoundResource<ResultType : Any, RequestType : Any> @MainThread
internal constructor(private val appExecutors: AppExecutors) {

    private val result = MediatorLiveData<Resource<ResultType>>()

    init {
        val dbSource = loadFromDb(true)
        result.addSource(dbSource) { data ->
            result.removeSource(dbSource)
            if (shouldFetch(data)) {
                result.value = Resource.loading(data)
                fetchFromNetwork(dbSource)
            } else {
                result.addSource(dbSource) { newData -> setValue(Resource.success(newData)) }
            }
        }
    }

    @MainThread
    private fun setValue(newValue: Resource<ResultType>) {
        if (result.value != newValue) {
            result.value = newValue
        }
    }

    private fun fetchFromNetwork(dbSource: LiveData<ResultType>) {
        val apiResponse = createCall()
        result.addSource(dbSource) { newData -> setValue(Resource.loading(newData)) }
        result.addSource(apiResponse) { response ->
            result.removeSource(apiResponse)
            result.removeSource(dbSource)

            if (response?.isSuccessful ?: false) {
                appExecutors.diskIO.execute({
                    saveCallResult(processResponse(response!!)!!)
                    appExecutors.mainThread.execute({
                        result.addSource(loadFromDb(false)
                        ) { newData -> setValue(Resource.success(newData)) }
                    })
                })
            } else {
                onFetchFailed()
                result.addSource(dbSource
                ) { newData -> setValue(Resource.error(response?.errorMessage, newData)) }
            }
        }
    }

    protected open fun onFetchFailed() {}

    fun asLiveData(): LiveData<Resource<ResultType>> = result

    @WorkerThread
    protected fun processResponse(response: ApiResponse<RequestType>) = response.body

    @WorkerThread
    protected abstract fun saveCallResult(item: RequestType)

    @MainThread
    protected abstract fun shouldFetch(data: ResultType?): Boolean

    @MainThread
    protected abstract fun loadFromDb(preFetch: Boolean): LiveData<ResultType>

    @MainThread
    protected abstract fun createCall(): LiveData<ApiResponse<RequestType>>

    @MainThread
    protected fun onFinished() = {}
}
