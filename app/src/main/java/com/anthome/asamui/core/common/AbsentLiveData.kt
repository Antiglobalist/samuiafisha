package com.anthome.asamui.core.common

import android.arch.lifecycle.LiveData

class AbsentLiveData : LiveData<Any>() {
    init {
        postValue(null)
    }

    companion object {
        fun create() = AbsentLiveData()
    }
}