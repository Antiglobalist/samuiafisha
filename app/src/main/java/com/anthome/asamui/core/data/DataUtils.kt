package com.anthome.asamui.core.data

import com.anthome.asamui.core.data.utils.ListParameterizedType
import com.google.gson.Gson
import java.util.*


inline fun <reified T : Any> stringToList(data: String?): List<T> {
    val gson = Gson()
    if (data == null) {
        return Collections.emptyList()
    }

    val listType = ListParameterizedType(T::class.java)

    return gson.fromJson(data, listType)
}

fun <T : Any> listToString(list: List<T>): String {
    val gson = Gson()
    return gson.toJson(list)
}