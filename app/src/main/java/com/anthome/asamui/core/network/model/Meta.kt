package com.anthome.asamui.core.network.model

import com.google.gson.annotations.SerializedName


class Meta {

    @SerializedName("new_offset")
    var new_offset: Int = 0
}