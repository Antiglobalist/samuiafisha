package com.anthome.asamui.core.data

import android.arch.persistence.room.Database
import android.arch.persistence.room.RoomDatabase
import android.arch.persistence.room.TypeConverters

import com.anthome.asamui.core.data.dao.EventsDao
import com.anthome.asamui.core.model.Event

@Database(entities = arrayOf(Event::class), version = 1)
@TypeConverters(Converters::class)
abstract class AppDataBase : RoomDatabase() {

    abstract fun eventsDao(): EventsDao

}