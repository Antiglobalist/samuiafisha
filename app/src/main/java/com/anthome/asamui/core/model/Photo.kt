package com.anthome.asamui.core.model


interface Photo {
    fun getPath() : String
}