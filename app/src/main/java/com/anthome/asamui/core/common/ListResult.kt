package com.anthome.asamui.core.common

class ListResult<T>(val list: List<T>?, var hasMore: Boolean,  var nextAvailable: Boolean) {

    val offset: Int

    init {
        offset = list?.size ?: 0
    }
}
