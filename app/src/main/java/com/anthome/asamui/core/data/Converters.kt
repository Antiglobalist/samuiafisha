package com.anthome.asamui.core.data

import android.arch.persistence.room.TypeConverter
import com.anthome.asamui.core.model.Comment
import com.anthome.asamui.core.model.Event


class Converters {

    @TypeConverter
    fun stringToPhotoList(data: String?): List<Event.PhotoEvent> = stringToList(data)

    @TypeConverter
    fun photoListToString(list: List<Event.PhotoEvent>): String = listToString(list)

    @TypeConverter
    fun stringToCommentList(data: String?): List<Comment> = stringToList(data)

    @TypeConverter
    fun commentListToString(list: List<Comment>): String = listToString(list)
}