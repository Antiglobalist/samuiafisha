package com.anthome.asamui.core.network

import android.content.Context
import com.anthome.asamui.BuildConfig
import com.readystatesoftware.chuck.ChuckInterceptor
import okhttp3.OkHttpClient
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.TimeUnit
import javax.inject.Inject
import javax.inject.Singleton


//TODO Запилить ремоут для хоста
@Singleton
class ApiClient
@Inject constructor(context: Context) {

    val api: Api

    init {
        val okHttpClientBuilder = OkHttpClient().newBuilder()
                .readTimeout(15, TimeUnit.SECONDS)
                .writeTimeout(15, TimeUnit.SECONDS)
        if (BuildConfig.DEBUG) {
            //val logging = HttpLoggingInterceptor()
            //logging.level = HttpLoggingInterceptor.Level.BODY
            //okHttpClientBuilder.addInterceptor(logging)
            okHttpClientBuilder.addInterceptor(ChuckInterceptor(context))
        }
        val okHttpClient = okHttpClientBuilder.build()

        val retrofit = Retrofit.Builder()
                .addConverterFactory(GsonConverterFactory.create())
                .addCallAdapterFactory(LiveDataCallAdapterFactory())
                .client(okHttpClient)
                .baseUrl("http://81.177.160.135/afisha/hs/")
                .build()

        api = retrofit.create(Api::class.java)
    }

    /*private inner class AuthInterceptor : Interceptor {
        @Throws(IOException::class)
        override fun intercept(chain: Interceptor.Chain): okhttp3.Response {
            var result = chain.request()
            if (!TextUtils.isEmpty(authorized)) {
                result = result.newBuilder()
                        .addHeader("Authorization", authorized)
                        .build()
            }
            return chain.proceed(result)
        }
    }

    private inner class TokenRefreshInterceptor : Interceptor {
        @Throws(IOException::class)
        override fun intercept(chain: Interceptor.Chain): okhttp3.Response {
            val request = chain.request()
            var response: okhttp3.Response = chain.proceed(request)
            if (response.code() == 401) {
                try {
                    refreshToken()
                } catch (e: LoginRequiredException) {
                    e.printStackTrace()
                }
                //TODO изменить токен у неудачного запроса
                response = chain.proceed(request)
            }

            return response
        }
    }
*/
}