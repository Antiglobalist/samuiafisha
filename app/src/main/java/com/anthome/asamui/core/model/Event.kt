package com.anthome.asamui.core.model

import android.arch.persistence.room.Embedded
import android.arch.persistence.room.Entity
import com.anthome.asamui.utils.extensions.emptyString
import com.google.gson.annotations.SerializedName

@Entity(tableName = "events", primaryKeys = ["id", "date"])
data class Event(
        @SerializedName("id") var id: Int, //000000016
        @SerializedName("name") var name: String = emptyString, //Walking street, Chaweng
        @SerializedName("date") var date: String = emptyString, //11.04.2018 18:00:00
        @SerializedName("description") var description: String = emptyString,
        @Embedded(prefix = "location_")
        @SerializedName("location") var location: Location? = null,
        @SerializedName("calendar") var calendar: String = emptyString, //Пн, Вт, Ср, Чт, Сб с 18:00 - 22:00
        @SerializedName("price") var price: String = emptyString,
        @SerializedName("person") var person: String = emptyString,
        @SerializedName("phone") var phone: String = emptyString,
        @SerializedName("type") var type: String = emptyString, //Ярмарка
        @SerializedName("language") var language: String = emptyString, //Русский
        @SerializedName("liked") var liked: Boolean = false, //false
        @SerializedName("bookmark") var bookmark: Boolean = false, //false
        @SerializedName("likes") var likes: Int = 0, //3
        @SerializedName("photos") var photos: List<PhotoEvent> = listOf(),
        @SerializedName("comments") var comments: List<Comment> = listOf()
) {

    fun getCommentCount() = comments.size

    data class Location(
            @SerializedName("name") var name: String = emptyString, //Koh Samui, 39/12 Soi Chumchon Chaweng Yai 13
            @SerializedName("longitude") var longitude: String = emptyString, //100,05687562339706
            @SerializedName("latitude") var latitude: String = emptyString //9522335772165615
    )

    data class PhotoEvent(
            @SerializedName("url") var url: String = emptyString, //https://firebasestorage.googleapis.com/v0/b/afishasamui.appspot.com/o/images%2F322E2693-4CEF-45D0-AB10-22F7FCB7ACDC.jpeg?alt=media&token=f7e2f6d9-fd19
            @SerializedName("gs") var gs: Boolean = false //true
    ) : Photo {
        override fun getPath() = url
    }
}