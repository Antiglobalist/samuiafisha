package com.anthome.asamui.utils.extensions

import android.app.Activity
import android.os.Build
import android.support.v4.app.Fragment
import android.widget.ImageView
import android.widget.TextView
import android.widget.Toast
import com.anthome.asamui.core.model.Photo
import com.anthome.asamui.utils.GlideApp

fun ImageView.setImagePhoto(photo: Photo?, inside: Boolean = false) {
    val request = GlideApp.with(this).load(photo?.getPath())
    if (inside) request.centerInside()
    request.into(this)
}

fun TextView.setString(resId: Int, vararg args: Any) {
    this.setText(this.context.getString(resId, args))
}

fun Fragment.showToast(message: CharSequence, duration: Int = Toast.LENGTH_SHORT) {
    Toast.makeText(getActivity(), message, duration).show()
}

fun Activity.showToast(message: CharSequence, duration: Int = Toast.LENGTH_SHORT) {
    Toast.makeText(this, message, duration).show()
}

inline fun supportsLollipop(code: () -> Unit) {
    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
        code()
    }
}