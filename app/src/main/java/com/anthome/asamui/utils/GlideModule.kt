package com.anthome.asamui.utils

import android.content.Context
import android.graphics.drawable.Drawable
import com.anthome.asamui.R
import com.bumptech.glide.GlideBuilder
import com.bumptech.glide.annotation.GlideModule
import com.bumptech.glide.load.resource.drawable.DrawableTransitionOptions
import com.bumptech.glide.module.AppGlideModule
import com.bumptech.glide.request.RequestOptions

@GlideModule
class GlideModuleImpl : AppGlideModule() {

    override fun applyOptions(context: Context, builder: GlideBuilder) {
        val opt = RequestOptions()
                .centerCrop().placeholder(R.color.grayLight).error(R.color.transparent)
        builder
                .setDefaultRequestOptions(opt)
                .setDefaultTransitionOptions(Drawable::class.java, DrawableTransitionOptions().crossFade())
    }
}