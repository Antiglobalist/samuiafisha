package com.anthome.asamui.utils.databinding

import android.databinding.BindingConversion
import android.view.View


@BindingConversion
fun convertIntToBoolean(value: Int): Boolean {
    return value == 1
}

@BindingConversion
fun convertBooleanToVisibility(visible: Boolean): Int {
    return if (visible) View.VISIBLE else View.GONE
}