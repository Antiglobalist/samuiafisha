package com.anthome.asamui.utils.extensions

import android.animation.AnimatorInflater
import android.support.v4.content.ContextCompat
import android.view.View


fun View.getDrawable(resId: Int) = ContextCompat.getDrawable(this.context, resId);
fun View.getColor(resId: Int) = ContextCompat.getColor(this.context, resId);
fun View.getPixelSize(resId: Int) = context.resources.getDimensionPixelSize(resId);
fun View.getStateAnimator(resId: Int) = AnimatorInflater.loadStateListAnimator(context, resId)
fun View.notEditMode(code: () -> Unit) {
    if (!isInEditMode) {
        code()
    }
}