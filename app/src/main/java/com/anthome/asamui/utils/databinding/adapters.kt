package com.anthome.asamui.utils.databinding

import android.databinding.BindingAdapter
import android.support.v4.widget.SwipeRefreshLayout
import android.widget.ImageView
import com.anthome.asamui.core.model.Event
import com.anthome.asamui.core.model.Photo
import com.anthome.asamui.ui.viewmodel.ProcessViewModelState
import com.anthome.asamui.utils.extensions.setImagePhoto

@BindingAdapter("refreshing")
fun setRefreshing(view: SwipeRefreshLayout, refreshing: Boolean) {
    view.isRefreshing = refreshing
}

@BindingAdapter("refreshing")
fun setRefreshing(view: SwipeRefreshLayout, state: ProcessViewModelState?) {
    setRefreshing(view, state == ProcessViewModelState.LOADING)
}

@BindingAdapter("color")
fun setProgressColor(view: SwipeRefreshLayout, color: Int) {
    view.setColorSchemeColors(color)
}

@BindingAdapter("images")
fun setImage(view: ImageView, photos: List<Event.PhotoEvent>?) {
    var photo: Photo? = null;
    if (photos != null && photos.isNotEmpty()) {
        photo = photos.get(0)
    }
    view.setImagePhoto(photo)
}