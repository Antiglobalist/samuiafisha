package com.anthome.asamui.utils.extensions

const val emptyString: String = ""

inline fun <T:Any, R> notNull(input: T?, callback: (T)->R): R? {
    return input?.let(callback)
}
