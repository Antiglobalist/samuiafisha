package com.anthome.asamui.di.module

import android.app.Application
import android.arch.persistence.room.Room
import android.content.Context
import com.anthome.asamui.core.data.AppDataBase
import com.anthome.asamui.core.data.dao.EventsDao
import com.anthome.asamui.core.network.Api
import com.anthome.asamui.core.network.ApiClient
import dagger.Module
import dagger.Provides
import javax.inject.Singleton


@Module(includes = [ViewModelModule::class])
class AppModule() {

    @Provides
    @Singleton
    fun context(app: Application): Context {
        return app
    }

    @Provides
    @Singleton
    fun provideApi(client: ApiClient): Api = client.api

    @Provides
    @Singleton
    fun provideDb(app: Application): AppDataBase {
        return Room.databaseBuilder(app, AppDataBase::class.java, "appDataBase.db").build()
    }

    @Provides
    @Singleton
    fun provideEventsDao(db: AppDataBase): EventsDao {
        return db.eventsDao()
    }
}