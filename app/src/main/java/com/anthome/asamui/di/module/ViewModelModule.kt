package com.anthome.asamui.di.module

import android.arch.lifecycle.ViewModel
import android.arch.lifecycle.ViewModelProvider
import com.anthome.asamui.ui.screens.events.EventsListViewModel
import com.anthome.asamui.ui.viewmodel.ViewModelFactory
import dagger.Binds
import dagger.Module
import dagger.multibindings.IntoMap



@Module
abstract class ViewModelModule {

    @Binds
    @IntoMap
    @ViewModelKey(EventsListViewModel::class)
    internal abstract fun bindEventsViewModel(eventsViewModel: EventsListViewModel): ViewModel

    @Binds
    internal abstract fun bindViewModelFactory(factory: ViewModelFactory): ViewModelProvider.Factory
}