package com.anthome.asamui.di.module

import com.anthome.asamui.ui.screens.events.EventsListFragment
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Module
abstract class FragmentBuildersModule {

    @ContributesAndroidInjector
    abstract fun contributeListFragment(): EventsListFragment
}