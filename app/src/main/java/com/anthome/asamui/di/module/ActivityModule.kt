package com.anthome.asamui.di.module

import com.anthome.asamui.ui.screens.events.EventsListActivity
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Module
abstract class ActivityModule {

    @ContributesAndroidInjector(modules = [FragmentBuildersModule::class])
    abstract fun contributeEventsListActivity(): EventsListActivity
}
